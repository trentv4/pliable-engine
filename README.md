# Pliable Engine #

A lightweight, extendable, and most importantly *pliable* (see: malleable, easily-changed) game engine.

### History ###

This project is the result of several months of co-development and refinement through my Roguelike and Top-down shooter projects (available on bitbucket.org/trentv4). 

### How do I get set up? ###

* Open the project in your preferred IDE. The libraries used (LWJGL) are distributed with it. Natives are shipped alongside it.

### Contribution guidelines ###

* All contributions must be well documented, well-written, and easy to understand and modify.

### Who do I talk to? ###

* Trent VanSlyke. Found here: trentvanslyke@gmail.com, @trentv4, twitter.com/trentv4, jamieswhiteshirt.com/trentv4