package com.trentv4.pliable;

/** Basic structure class. It must be added to the stack in GameLoop. */
public class GameStructure
{
	public boolean isInitialized = false;
	protected InputScenario INPUT_SCENARIO;

	/**
	 * Method called once before any ticks, used for initializing the game
	 * state.
	 */
	public void initialize()
	{
	}

	/** Method called from DisplayManager, used for rendering the state. */
	public void draw(Renderer renderer)
	{
	}

	/** Primary state updater. */
	public void tick()
	{
	}
}
