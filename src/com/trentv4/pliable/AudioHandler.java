package com.trentv4.pliable;

import java.io.File;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public final class AudioHandler
{
	/**
	 * Plays a sound clip located at path. This returns a Clip, so you can stop/pause it at any time you want.
	 */
	public static final Clip playSound(String path)
	{
		try
		{
			AudioInputStream audioIn = AudioSystem.getAudioInputStream(new File("sounds/" + path));
			Clip clip = AudioSystem.getClip();
			clip.open(audioIn);
			clip.start();
			return clip;
		} catch (Exception e)
		{
			Logger.log(LogLevel.ERROR, "Unable to play sound: " + path);
			return null;
		}
	}
}
