package com.trentv4.pliable;

/**
 * Main class of the program. This handles the life status of the program, all
 * tracked threads, stores the program directory, and provides several useful
 * methods in the the general execution of the program.
 */
public class MainGame
{
	private static boolean isRunning = true;
	private static String path;
	public static final String ENGINE_VERSION = "1.2 RELEASE";
	private static final int TICK_RATE = 60;

	/** Main entry point of the program. Accepts no arguments. */
	public static void main(String[] args)
	{
		Logger.initialize(LogLevel.INIT_NOTE);
		try
		{
			path = System.getProperty("user.dir") + "/";
			Logger.log(LogLevel.INIT_NOTE, "Created path at " + path);
			Thread t = new Thread(new GameThread());
			t.setName("Logic");
			t.start();
			DisplayManager.initialize();
		} catch (Exception e)
		{
			crash(e);
		}
	}

	/** Kills the program. */
	public static void kill()
	{
		Logger.log("Program is being killed.");
		isRunning = false;
		Logger.saveLog("log.txt");
		System.exit(0); // I ain't havin' no leakin' threads on my watch!
	}

	/** Prints the Exception and then kills the program. */
	public static final void crash(Exception e)
	{
		Logger.log("Program has crashed! Printing error log: ");
		e.printStackTrace();
		kill();
	}

	/**
	 * Checks the status of the program (if executing or not). This allows for
	 * safe, <i>ConcurrentModificationException</i>-free heartbeat checking.
	 */
	public static boolean isAlive()
	{
		return isRunning;
	}

	/**
	 * Returns the current directory, ready-formatted for use, e.g.
	 * "<i>/home/trent/execution_folder/</i>".
	 */
	public static String getPath()
	{
		return path;
	}

	static class GameThread implements Runnable
	{
		@Override
		public void run()
		{
			Logger.log(LogLevel.INIT_NOTE, "Game thread started.");
			long resolution = 1000000000 / TICK_RATE;
			long startTime = System.nanoTime();
			long currentTime = startTime;
			while (isRunning)
			{
				currentTime = System.nanoTime();
				if (currentTime - startTime > resolution)
				{
					startTime = System.nanoTime();
					currentTime = startTime;
					GameLoop.run();
				}
			}
		}
	}
}
