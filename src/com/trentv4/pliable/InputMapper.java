package com.trentv4.pliable;

/**
 * Input class for all keyboard and mouse inputs. This class is called from
 * DisplayManager, where it receives events from the glfw window. This class has
 * several important methods:
 * <ul>
 * <li><i>initialize()</i>, which will initialize all variables,</li>
 * <li><i>setMousePos(x, y)</i>, which will set the internal variables to {x,y},
 * </li>
 * <li><i>setMouseUp(key)</i>, which sets a tracked mouse button as "up" or
 * false,</li>
 * <li><i>setMouseDown(key)</i>, which sets a tracked mouse button as "down" or
 * true,</li>
 * <li><i>tick()</i>, which is called from GameLoop once per iteration,</li>
 * <li><i>setUp(key)</i>, which sets a tracked key as "up" or false,</li>
 * <li><i>setDown(key)</i>, which sets a tracked key as "down" or true,</li>
 * <ul>
 * */
public class InputMapper
{
	private static int[] mousePos = new int[2];
	private static boolean[] mouseButtons = new boolean[10];
	private static int wheelDelta = 0;

	private static boolean[] keyHeldStatus = new boolean[348];
	private static boolean[] keyPressedStatus = new boolean[348];

	/** Sets the current mouse position to {x, y}. */
	public static final void setMousePos(int x, int y)
	{
		mousePos[0] = x;
		mousePos[1] = y;
	}

	public static final void setMouseWheelDelta(int delta)
	{
		wheelDelta = delta;
	}

	/** Sets the provides <i>button</i> to "true". Used for the mouse.` */
	public static final void setMouseDown(int button)
	{
		if (mouseButtons.length >= button)
		{
			mouseButtons[button] = true;
		}
	}

	/** Sets the provides <i>button</i> to "false". */
	public static final void setMouseUp(int button)
	{
		if (mouseButtons.length >= button)
		{
			mouseButtons[button] = false;
		}
	}

	public static final boolean[] getMouseButtons()
	{
		return mouseButtons;
	}

	public static final int getWheel()
	{
		return wheelDelta;
	}

	public static final int[] getMousePos()
	{
		return mousePos;
	}

	public static final boolean isPressed(int key)
	{
		if (keyPressedStatus[key])
		{
			keyPressedStatus[key] = false;
			return true;
		}
		return false;
	}

	public static final boolean isDown(int key)
	{
		return keyHeldStatus[key];
	}

	public static final void setStatus(int key, boolean state)
	{
		try
		{
			keyHeldStatus[key] = state;
			if (state)
			{
				keyPressedStatus[key] = true;
			}
		} catch (Exception e)
		{
			// whoops!
		}
	}
}
